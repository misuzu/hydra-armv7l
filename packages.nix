{ nixpkgs ? <nixpkgs>
, full ? true
}:
let
  pkgsAarch64 = import nixpkgs {
    system = "aarch64-linux";
  };
  pkgs = import nixpkgs {
    system = "armv7l-linux";
    config.allowUnfree = true;
  };
in
rec {
  inherit (pkgs)
    # cachix
    # clang
    # clang_11
    # clang_12
    # clang_13
    # clang_14
    # gcc
    # gcc10
    # gcc11
    # gcc12
    # gcc9
    # llvm
    # llvm_11
    # llvm_12
    # llvm_13
    # llvm_14
    # nix-diff
    # nodejs
    # pandoc
    _3proxy
    age
    alacritty
    alfis-nogui
    alsa-firmware
    alsa-utils
    arc-theme
    aria2
    asciidoc
    aspell
    atuin
    autoconf
    automake
    bash
    bat
    bcache-tools
    bind
    binutils
    bison
    bluez
    bluez-alsa
    bluez-tools
    bootspec
    broot
    btrfs-progs
    busybox
    bzip2
    cairo
    capitaine-cursors
    cargo
    ccrypt
    cifs-utils
    ckbcomp
    cloudflared
    clutter
    cmake
    cmus
    cogl
    connman
    coreutils
    coreutils-full
    criu
    crun
    cryptsetup
    curl
    db
    dbus
    dbus-broker
    dconf
    ddrescue
    dejavu_fonts
    direnv
    dmenu
    docbook-xsl-ns
    docbook5
    docker
    docker-compose
    dtc
    e2fsprogs
    efibootmgr
    enchant
    exiv2
    expat
    expect
    faba-icon-theme
    fakeroot
    feh
    ffmpeg
    ffmpeg-full
    ffmpeg_5
    file
    flex
    freetype
    fzf
    gdb
    getconf
    gettext
    ghc
    git
    gixy
    glibcLocales
    glibcLocalesUtf8
    glxinfo
    gmp
    gnome-icon-theme
    gnupg
    gnutls
    go
    gparted
    graphene
    grml-zsh-config
    grub2_efi
    grub2_light
    gsl
    gspell
    guile
    hatch
    hello
    help2man
    htop
    hunspell
    i2pd
    ibus
    iconnamingutils
    inetutils
    inkscape
    iotop
    iotop-c
    isl
    iw
    iwd
    jfsutils
    jnettop
    jq
    headscale
    kmon
    lego
    less
    lesspipe
    lib2geom
    libelf
    liberation_ttf
    liberation_ttf_v1
    libffi
    libmpc
    libressl
    librevenge
    libsecret
    libuv
    libxcrypt
    libxml2
    libxslt
    light
    lightdm
    lightdm-enso-os-greeter
    linux-firmware
    lirc
    logrotate
    lorri
    lsof
    lua
    makeInitrdNGTool
    man-pages
    maturin
    mc
    mdadm
    mdbook
    mesa
    meson
    minica
    mkpasswd
    moc
    moka-icon-theme
    mosquitto
    mpc_cli
    mpd
    mpfr
    mpv
    ms-sys
    mtdutils
    mtools
    mympd
    mypy
    nano
    ncmpcpp
    ncurses
    neofetch
    netavark
    netbird
    netdata
    networkmanager
    nfs-utils
    nginx
    ninja
    nix
    nix-info
    nixos-install-tools
    nixos-option
    nixos-rebuild
    nixpkgs-review
    nmap
    nnn
    noto-fonts
    noto-fonts-emoji
    npth
    nsncd
    ntfs3g
    openssh
    openssl
    openvpn
    p7zip
    pango
    papirus-icon-theme
    patchelf
    pavucontrol
    pciutils
    pcmanfm
    pcre
    perl
    picom
    pipewire
    pkg-config
    plymouth
    podman
    prefetch-npm-deps
    protobuf
    pulseaudio
    pulseaudio-ctl
    pulseaudioFull
    python3
    rage
    readline
    reiserfsprogs
    remarshal
    ronn
    ruby
    rustc
    samba
    screen
    scrot
    sdparm
    shellcheck-minimal
    slock
    smartmontools
    socat
    sof-firmware
    spectrwm
    speedtest-cli
    sqlite
    squashfs-tools-ng
    squashfsTools
    sshfs
    starship
    stdenv
    strace
    strongswan
    sudo
    swig
    symbola
    sysstat
    systemd
    systemdMinimal
    tailscale
    tcl
    tcp_wrappers
    tcpdump
    terminus_font
    testdisk
    texinfo
    texinfoInteractive
    time
    tio
    tk
    tmux
    traceroute
    tree
    txt2tags
    ubuntu_font_family
    unclutter
    unicode-character-database
    unicode-emoji
    unrar
    unzip
    usbutils
    util-linux
    valgrind
    vanilla-dmz
    vim
    w3m
    w3m-nox
    wakeonlan
    wget
    which
    whois
    wireguard-tools
    wirelesstools
    wireplumber
    wpa_supplicant
    wqy_microhei
    wqy_zenhei
    xorriso
    xss-lock
    xterm
    xz
    yggdrasil
    ympd
    zerotierone
    zigbee2mqtt
    zlib
    zram-generator
    zsh
    ;
} // pkgs.lib.optionalAttrs full rec {
  minimalExample = (pkgs.nixos ({ config, lib, pkgs, modulesPath, ... }: {
    imports = [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

    fileSystems."/".device  = pkgs.lib.mkDefault "/dev/sda1";

    boot.loader.efi.efiSysMountPoint = "/boot/efi";
    boot.loader.grub.device = "nodev";
    boot.loader.grub.efiSupport = true;

    services.openssh.enable = true;

    system.includeBuildDependencies = true;
    system.stateVersion = "23.05";
  })).config.system.build.toplevel;

  graphicalExample = (pkgs.nixos ({ config, lib, pkgs, modulesPath, ... }: {
    imports = [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

    fileSystems."/".device  = pkgs.lib.mkDefault "/dev/sda1";

    boot.loader.efi.efiSysMountPoint = "/boot/efi";
    boot.loader.grub.device = "nodev";
    boot.loader.grub.efiSupport = true;

    boot.kernelPackages = lib.mkForce config.boot.zfs.package.latestCompatibleLinuxPackages;
    boot.supportedFilesystems = [ "ntfs" ];

    hardware.bluetooth.enable = true;

    hardware.pulseaudio.enable = true;

    networking.networkmanager.enable = true;

    services.xserver.enable = true;
    services.xserver.desktopManager.xterm.enable = false;
    services.xserver.displayManager.lightdm.enable = true;
    services.xserver.windowManager.spectrwm.enable = true;
    services.xserver.libinput.enable = true;

    services.openssh.enable = true;

    system.includeBuildDependencies = true;
    system.stateVersion = "23.05";
  })).config.system.build.toplevel;

  # clang_git = pkgs.llvmPackages_git.clang;
  # llvm_git = pkgs.llvmPackages_git.llvm;

  # clang_latest = pkgs.llvmPackages_latest.clang;
  # llvm_latest = pkgs.llvmPackages_latest.llvm;

  # clang_15 = pkgs.llvmPackages_15.clang;
  # llvm_15 = pkgs.llvmPackages_15.llvm;

  # clang_16 = pkgs.llvmPackages_16.clang;
  # llvm_16 = pkgs.llvmPackages_16.llvm;

  # linux_5_15 = pkgs.linuxPackages_5_15.kernel;
  # linux_5_15_cpupower = pkgs.linuxPackages_5_15.cpupower;
  # linux_5_15_rtl8189es = pkgs.linuxPackages_5_15.rtl8189es;
  # linux_5_15_usbip = pkgs.linuxPackages_5_15.usbip;

  linux_5_10 = pkgs.linuxPackages_5_10.kernel;
  linux_5_10_cpupower = pkgs.linuxPackages_5_10.cpupower;
  # linux_5_10_rtl8189es = pkgs.linuxPackages_5_10.rtl8189es;
  linux_5_10_usbip = pkgs.linuxPackages_5_10.usbip;

  linux_default = pkgs.linuxPackages.kernel;
  linux_default_cpupower = pkgs.linuxPackages.cpupower;
  linux_default_rtl8189es = pkgs.linuxPackages.rtl8189es;
  linux_default_usbip = pkgs.linuxPackages.usbip;

  linux_latest = pkgs.linuxPackages_latest.kernel;
  linux_latest_cpupower = pkgs.linuxPackages_latest.cpupower;
  linux_latest_rtl8189es = pkgs.linuxPackages_latest.rtl8189es;
  linux_latest_usbip = pkgs.linuxPackages_latest.usbip;

  linux_latest_zfs = pkgs.zfs.latestCompatibleLinuxPackages.kernel;
  linux_latest_zfs_cpupower = pkgs.zfs.latestCompatibleLinuxPackages.cpupower;
  linux_latest_zfs_rtl8189es = pkgs.zfs.latestCompatibleLinuxPackages.rtl8189es;
  linux_latest_zfs_usbip = pkgs.zfs.latestCompatibleLinuxPackages.usbip;

  linux_default_compat_uts_machine = pkgsAarch64.linuxPackages.kernel.override (originalArgs: {
    kernelPatches = (originalArgs.kernelPatches or []) ++ [
      rec {
        name = "compat_uts_machine";
        patch = pkgsAarch64.fetchpatch {
          inherit name;
          url = "https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/jammy/patch/?id=c1da50fa6eddad313360249cadcd4905ac9f82ea";
          sha256 = "sha256-357+EzMLLt7IINdH0ENE+VcDXwXJMo4qiF/Dorp2Eyw=";
        };
      }
    ];
  });
  linux_default_compat_uts_machine_zfs = (pkgsAarch64.linuxPackagesFor linux_default_compat_uts_machine).zfs;

  inherit (pkgs.gnome)
    gdm
    gnome-terminal
    mutter
    nautilus
    ;

  inherit (pkgs.libsForQt5)
    kwin
    sddm
    ;

  inherit (pkgs.xorg)
    xorgserver
    xrandr
    ;
}
